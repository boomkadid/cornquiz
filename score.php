<!DOCTYPE html>
<html>
<head>
<link href="stylesheets/all.css?version=51" rel="stylesheet" type="text/css" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
</head>
<body>
<?php
$conn = new mysqli("localhost","root", "", "cornquiz");
$sql = "SELECT * FROM expert WHERE 1";
$result = $conn->query($sql);
$answer =  array();
$list_name = array();
$list_acc = array();
$type_ans = array(0,0,0,0,0,0,0,0,0,0,0);
$type_sol = array(0,0,0,0,0,0,0,0,0,0,0);
$type_total = array(0,0,0,0,0,0,0,0,0,0,0);
if ($result->num_rows==0) echo "no result";
else{
    while($row = mysqli_fetch_assoc($result)) {
      array_push($list_name,$row['name']);
	  array_push($answer, [$row['name'],$row['answer'],$row['sol1']]);
    }
}
for($i=0;$i<count($answer);$i++){
	$sol = explode(" ", $answer[$i][2]);
	$ans = explode(" ", $answer[$i][1]);
	$score = 0;
	for($j=0;$j<count($sol);$j++){
		$type_sol[$sol[$j]] += 1;
		if ($ans[$j+1] == $sol[$j])
			$type_ans[$ans[$j+1]] += 1;
			$score += 1;
	}
		array_push($list_acc, $score);
}	
	// for($i=0;$i<11;$i++) $type_total[$i] = $type_ans[$i]/$type_sol[$i];
	$list_name = implode("','", $list_name);
	$list_acc = implode(",", $list_acc);
	$type_total = implode(",", $type_ans);
?>
<br><br>
<div id="container" name="chart1" style="min-width: 310px; height: 400px; margin: 0 auto"></div><br><br>
<div id="container" name="chart2" style="min-width: 310px; height: 400px; margin: 0 auto"></div><br><br>
<div id="container" name="chart3" style="min-width: 310px; height: 400px; margin: 0 auto"></div><br><br>
<div id="container" name="chart4" style="min-width: 310px; height: 400px; margin: 0 auto"></div><br><br>
<script>
$(function () {
    $('[name=chart1]').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Accuracy'
        },
        subtitle: {
            text: 'madlab.cpe.ku.ac.th/cornquiz'
        },
        xAxis: {
            categories: <?php echo "['".$list_name."']"; ?>,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'scores(%)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'score',
             // data: [49.9, 71.5, 106.4, 129.2]
            data: <?php echo "[".$list_acc."]"; ?>

        }]
    });
});
$(function () {
    $('[name=chart2]').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Accuracy'
        },
        subtitle: {
            text: 'madlab.cpe.ku.ac.th/cornquiz'
        },
        xAxis: {
            categories: ['Type1','Type2','Type3','Type4','Type5','Type6','Type7','Type8','Type9','Type10','Type11'],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'scores(%)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'score',
             // data: [49.9, 71.5, 106.4, 129.2]
            data: <?php echo "[".$type_total."]"; ?>

        }]
    });
});
$(function () {
    $('[name=chart3]').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Accuracy'
        },
        subtitle: {
            text: 'madlab.cpe.ku.ac.th/cornquiz'
        },
        xAxis: {
            categories: <?php echo "['".$list_name."']"; ?>,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'scores(%)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'score',
             // data: [49.9, 71.5, 106.4, 129.2]
            data: <?php echo "[".$list_acc."]"; ?>

        }]
    });
});
$(function () {
    $('[name=chart4]').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Accuracy'
        },
        subtitle: {
            text: 'madlab.cpe.ku.ac.th/cornquiz'
        },
        xAxis: {
            categories: <?php echo "['".$list_name."']"; ?>,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'scores(%)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'score',
             // data: [49.9, 71.5, 106.4, 129.2]
            data: <?php echo "[".$list_acc."]"; ?>

        }]
    });
});
</script>
</body>
</html>